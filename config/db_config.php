<?php
/**
 * Created by PhpStorm.
 * User: asifalamgir
 * Date: 16-06-11
 * Time: 2:55 PM
 */
const MongoUrl = '';

const PropertyTypeKey = 'PropertyType';
const PixUpdKey = 'Pix_updt';
const IdxDtKey = 'Idx_dt';
const Timestamp_sql = 'Timestamp_sql';
const PriceValue = "Lp_dol";

const Garage = "Gar";
const Kitchen = "num_kit";
const KitchenPlus = "Kit_plus";
const Maintenance = "Maint";
const OriginalPrice = "Orig_dol";
const ParkCost = "park_chgs";
const ParkSpaces = "park_spcs";
const Rooms = "Rms";
const RoomPlus = "Rooms_plus";
const MlsId = "Ml_num";
const TaxYear = "yr";
const Taxes = "taxes";
const Washrooms = "Bath_tot";

const BaseUrlKey = 'base_u_url';
const TotalPhoto = 'total_pic';
const HasPhotos = 'has_photos';


const IDX = 'idx';
const TeamIDX = 'team_listings';