<?php
const LOG_FILENAME = '/log/systemLog.txt';

abstract class FileType{
    const JPEG = 'image/jpeg';
    const JPEG_EXT = 'jpg';
    const PNG = "image/png";
    const PNG_EXT = 'png';
    const GIF = "image/gif";
    const GIF_EXT = 'gif';
}

abstract class ArgumentType {
    const UPDATED_PHOTOS = 'picture';
    const UPDATED_LISTINGS = 'listings';
    const ALL_PHOTOS = 'all_photos';
    const ALL_LISTINGS = 'all_listings';
    const TEAM_LISTINGS = 'team_listings';
}

abstract class PropertyType {
    const Residential = "ResidentialProperty";
    const Commercial = "CommercialProperty";
    const Condo = "CondoProperty";
}
?>
