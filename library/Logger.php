<?php


class Logger {
	private $fh;
	function __construct() {
		//do nothing in construction
		$this->fh = null;
	}
	public function logStatus($message) {
    	$currentTimestamp = date('Y-m-j H:i:s');//@TODO Verify this
    	if ($this->fh == null) {
    		//@TODO Open file for appending if exists. Otherwise create one.
    		//Keep FH open until connection is disconnected. Close it in disconnect call
    		$this->fh = fopen(dirname(dirname(__FILE__)).LOG_FILENAME, "a+") or die("Failed to create file");
    	}
    	$logMessage = "{$currentTimestamp} : {$message} \n";
    	
    	fwrite($this->fh, $logMessage); //@TODO VERIFY fwrite command
    	
    }
    public function closeFile() {
    	fclose($this->fh);
        $this->fh = null;
    }	
    
}
