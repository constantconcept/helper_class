<?php
//THIS FUNCTION IS IN A DIFFERENT CLASS FOR NOW, but this will be userful when filtering record 
class HelperClass {
	
	function __construct() {

	}
	public function getFilteredRecord($record) {
        if (isset($record[IdxDtKey])) {
            $record[IdxDtKey] = new MongoDate(strtotime($record[IdxDtKey]));
        }
		if (isset($record[PixUpdKey])) {
            $record[PixUpdKey] = new MongoDate(strtotime($record[PixUpdKey]));
        }
        if (isset($record[Timestamp_sql])) {
            $record[Timestamp_sql] = new MongoDate(strtotime($record[Timestamp_sql]));
        }
        if (isset($record[PriceValue])) {
            $record[PriceValue] = floatval($record[PriceValue]);
        }
        if (isset($record[Garage])) {
            $record[Garage] = floatval($record[Garage]);
        }
        if (isset($record[Kitchen])) {
            $record[Kitchen] = floatval($record[Kitchen]);
        }
        if (isset($record[KitchenPlus])) {
            $record[KitchenPlus] = floatval($record[KitchenPlus]);
        }
        if (isset($record[Maintenance])) {
            $record[Maintenance] = floatval($record[Maintenance]);
        }
        if (isset($record[OriginalPrice])) {
            $record[OriginalPrice] = floatval($record[OriginalPrice]);
        }
        if (isset($record[ParkCost])) {
            $record[ParkCost] = floatval($record[ParkCost]);
        }
        if (isset($record[ParkSpaces])) {
            $record[ParkSpaces] = floatval($record[ParkSpaces]);
        }
        if (isset($record[Rooms])) {
            $record[Rooms] = floatval($record[Rooms]);
        }
        if (isset($record[RoomPlus])) {
            $record[RoomPlus] = floatval($record[RoomPlus]);
        }
        if (isset($record[TaxYear])) {
            $record[TaxYear] = floatval($record[TaxYear]);
        }
        if (isset($record[Taxes])) {
            $record[Taxes] = floatval($record[Taxes]);
        }
        if (isset($record[Washrooms])) {
            $record[Washrooms] = floatval($record[Washrooms]);
        }

        $id = $record[MlsId];
        $base_url = '//admin.constantconcept.com/remax_idx/images/'.$id.'/';
        $record[BaseUrlKey] = $base_url;
        $record[HasPhotos] = $this->FileSaver->checkIfFileExists($id);
        $record[TotalPhoto] = $this->findNumberOfPhotosById($id);
        //find number of files
        
        return $record;
	}

}