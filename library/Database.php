<?php
/**
 * Created by PhpStorm.
 * User: asifalamgir
 * Date: 16-06-11
 * Time: 2:46 PM
 */
require_once(dirname(dirname(__FILE__)).'/config/db_config.php');

class Database extends Logger {
    private $mg;
    private $db;
    private $maxSaveLimit = 100;
    function __construct($dbName) {
    	parent::__construct();
        $this->mg = new MongoClient(MongoUrl);
        $this->db = $this->mg->{$dbName};
    }
    function updateListings($searchArray,$setArray,$upsert=false) {
        
        $collection = $this->db->Listings;
        //@TOD How to add upsert
        $collection->update($searchArray,array('$set'=>$setArray),array('upsert'=>true));
        echo "Update Complete\n";
    }
    function addToListingsBatch($dataArray) {
    	echo "+Starting DB Batch Insert, Current Batch ".sizeof($dataArray);
    	$nextBatch = $dataArray;
    	//Check the size of array
    	if (sizeof($dataArray) > $this->maxSaveLimit) {
    		$nextBatch = array_slice($dataArray,0,$this->maxSaveLimit); //@TODO VERIFY THIS COMMAND
    	} else {
    		$dataArray = array();
    	}
    	
    	$collection = $this->db->Listings;
    	//We might want to wait for this to complete
    	$collection->batchInsert(
    		$nextBatch,
    		array('continueOnError' => true)
    	);
    	//Recursively insert data
    	if (sizeof($dataArray) > 0) {
    		echo "- Data insert complete. Sleeping for one second and going for next round";
    		sleep(1);//lets go easy on the machine. Sleep for a second before the next batch
    		$this->addToListingsBatch($dataArray);
    	} else {
    		//I am done close logFile
    		$this->closeFile();
    	}
    }


}

?>
