<?php
/**
 * Created by PhpStorm.
 * User: asifalamgir
 * Date: 16-06-29
 * Time: 7:10 PM
 */

class FileSaver {
    private $rootDirectory;
    function __construct() {
        $this->rootDirectory = dirname(dirname(__FILE__)).'/images/';
    }
    public function checkIfFileExists($mlsId) {
    	return file_exists($this->rootDirectory.$mlsId);
    }
    public function getDirectory($mlsId) { 
    	return $this->rootDirectory.$mlsId."/";
    }
    public function createDirectory($mlsId) {
        if (!file_exists($this->rootDirectory.$mlsId)) {
            mkdir($this->rootDirectory.$mlsId , 0777, true);
        }
        return $this->rootDirectory.$mlsId;
    }
    public function saveFile($fileArray) {

        if (isset($fileArray['Success']) && $fileArray['Success'] == true) {
            $mlsId = $fileArray['Content-ID'];
            $index = $fileArray['Object-ID'];
            $fileName = $mlsId.'-'.$index.'.';
            switch ($fileArray['Content-Type']) {
                case FileType::JPEG:
                    $fileName .= FileType::JPEG_EXT;
                    break;
                case FileType::PNG:
                    $fileName .= FileType::PNG_EXT;
                    break;
                case FileType::GIF:
                    $fileName .= FileType::GIF_EXT;
                    break;
            }

            $dir = $this->createDirectory($mlsId);
            if (!file_exists($dir."/".$fileName)){
                file_put_contents($dir."/".$fileName, $fileArray['Data']);
            }

        }


    }
}
